using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace Amanotes
{
    public static class Utils
    {

        public static string GetMd5Hash(MD5 md5Hash, byte[] bytes)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(bytes);

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string ComputeHash(byte[] bytes)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = GetMd5Hash(md5Hash, bytes);
                return hash;
            }
        }

        public static string ComputeFileHash(string path) {
            var data = File.ReadAllBytes(path);
            var hash = ComputeHash(data);    
            return hash;
        }
    }
}
