using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
using System.IO;
using System.Linq;

namespace Amanotes.PackageManager
{
    [Serializable]
    public class DownloadMeta {
        public string name;
        public string path;
        public string hash;
        public DateTime downloadAt;
    }

    [Serializable]
    public class DownloadMetaList
    {
        public DownloadMeta[] Metas;
    }


    public class DownloadManager: EditorWindow
    {
        private static List<DownloadMeta> m_metas;

        private static string s_metaFile;

        static DownloadManager() 
        {
            s_metaFile = Path.Combine(Application.persistentDataPath, "downloads.json");
            
            if(File.Exists(s_metaFile)) {
                try {
                    var json = File.ReadAllText(s_metaFile);
                    var metaList = JsonUtility.FromJson<DownloadMetaList>(json);
                    m_metas = new List<DownloadMeta>(metaList.Metas);
                } catch (Exception e) {
                    m_metas = new List<DownloadMeta>();
                }
            } else {
                m_metas = new List<DownloadMeta>();
            }
        }

        static void Display()
        {
            UnityEditor.EditorWindow window = GetWindow(typeof(DownloadManager));
            window.Show();
        }

        public static IEnumerator DownloadFile(string url,  string filename, Action<string> callback)
        {

            var resultFile = GetLocalFilePath(filename);

            UnityWebRequest www = new UnityWebRequest(url);
          
            var dh = new DownloadHandlerFile(resultFile);
            dh.removeFileOnAbort = true;

            www.timeout = 60 * 60;
            www.downloadHandler = dh;

            Debug.Log("Start DownloadFile " + resultFile);
            yield return www.SendWebRequest();
          
            if(www.isNetworkError || www.isHttpError) {
                Debug.Log(www.error);
            } else if(callback != null) {
                while (!dh.isDone && !www.isNetworkError && Application.internetReachability != NetworkReachability.NotReachable )
                {
                    // Debug.Log(www.downloadProgress.ToString());
                    EditorUtility.DisplayProgressBar("Downloading Package",
                       filename, www.downloadProgress);
                    yield return null;
                }

                EditorUtility.ClearProgressBar();

                if (File.Exists(resultFile)) {

                    var hash = Utils.ComputeFileHash(resultFile);

                    var meta = m_metas.FirstOrDefault(e => e.name == filename);

                    if (meta == null)
                    {
                        meta = new DownloadMeta()
                        {
                            path = resultFile,
                            name = filename
                        };
                        m_metas.Add(meta);
                    }

                    meta.downloadAt = DateTime.Now;
                    meta.hash = hash;

                    var obj = new DownloadMetaList()
                    {
                        Metas = m_metas.ToArray()
                    };
                    var json = JsonUtility.ToJson(obj);
                    File.WriteAllText(s_metaFile, json);

                    callback(resultFile);
                }
               

            }
        }

        public static EditorCoroutine Download(string url, string fileName, Action<string> callback)
        {
            return EditorCoroutine.start(DownloadFile(url, fileName, callback));
        }

        public static DownloadMeta Find(string name)
        {
            return m_metas.FirstOrDefault(e => e.name == name);
        }

        public static string GetLocalFilePath(string filename) {
            return  Path.Combine(Application.persistentDataPath, filename);
        }

        public static bool IsValidLocalFile(string name) {
            var meta = Find(name);
            if(meta != null)
            {
                var filePath = GetLocalFilePath(name);
                if(File.Exists(filePath)) {
                    var hash = Utils.ComputeFileHash(filePath);
                    if(!string.IsNullOrEmpty(hash) && meta.hash == hash)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}