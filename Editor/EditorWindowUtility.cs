using System;
using UnityEditor;

namespace Amanotes.PackageManager
{

    internal static class EditorWindowUtility
    {
        public static void ShowInspectorEditorWindow()
        {
            string inspectorWindowTypeName = "UnityEditor.InspectorWindow";
            ShowEditorWindowWithTypeName(inspectorWindowTypeName);
        }

        public static void ShowSceneEditorWindow()
        {
            string sceneWindowTypeName = "UnityEditor.SceneView";
            ShowEditorWindowWithTypeName(sceneWindowTypeName);
        }

        public static void ShowTestEditorWindow()
        {
            string sceneWindowTypeName = "UnityEditor.UIAutomation.TestEditorWindow";
            ShowEditorWindowWithTypeName(sceneWindowTypeName);
        }

        public static void ShowEditorWindowWithTypeName(string windowTypeName)
        {
            var windowType = typeof(Editor).Assembly.GetType(windowTypeName);
            EditorWindow.GetWindow(windowType);
        }
    }
}