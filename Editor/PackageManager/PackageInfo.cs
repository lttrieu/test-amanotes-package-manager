using System;

namespace Amanotes.PackageManager
{
    [Serializable]
    public class PackageMeta
    {
        public string Name;
        public string Version;

        private int? versionCode;

        public int GetVersionCode() 
        {
            if(versionCode == null) {
                var outVal = 0;
                int.TryParse(Version.Replace(".", ""), out outVal);
                versionCode = outVal;
            }
            return versionCode.Value;
        }

        public string GetPackageName()
        {
            if(m_CachePackageName == null) {
                m_CachePackageName = string.Format("{0}_v{1}.unitypackage", Name, Version);
            }
            return m_CachePackageName;
        }

        private string m_CachePackageName;
    }


    [Serializable]
    public class PackageInfo : PackageMeta
    {
        public string Url;


        public PackageInfo[] Dependencies;
    }
}