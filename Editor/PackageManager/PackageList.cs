using System;
using System.Collections.Generic;
using UnityEngine;

namespace Amanotes.PackageManager
{
    [Serializable]
    public class PackageList
    {
        public PackageInfo[] Packages;
    }

    [Serializable]
    public class PackageMetaList
    {
        public PackageMeta[] Packages;
    }
}